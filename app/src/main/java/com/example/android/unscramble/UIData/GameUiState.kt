package com.example.android.unscramble.UIData

import kotlinx.coroutines.flow.MutableStateFlow

data class GameUiState(val currentScrambledWord: String = "") {

}